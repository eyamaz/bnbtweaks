package eyamaz.bnbtweaks.asm.reference;

import eyamaz.bnbtweaks.asm.Hooks;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class EntityMobModifications extends EntityMob
{
	// modified this.isValidLightLevel() to (Hooks.isSpawningFromSpawner || this.isValidLightLevel())
	@Override
	public boolean getCanSpawnHere()
	{
		return this.worldObj.difficultySetting != EnumDifficulty.PEACEFUL && (Hooks.isSpawningFromSpawner || this.isValidLightLevel()) && super.getCanSpawnHere();
	}

	// added a conditional at the top
	@Override
	public float getBlockPathWeight(int p_70783_1_, int p_70783_2_, int p_70783_3_)
	{
		// start injection
		if (Hooks.isSpawningFromSpawner)
			return 0f;
		// end injection
		return 0.5F - this.worldObj.getLightBrightness(p_70783_1_, p_70783_2_, p_70783_3_);
	}

	/*
	 * everything below is unmodified
	 * it is only required to avoid compilation errors
	 */
	public EntityMobModifications(World p_i1738_1_)
	{
		super(p_i1738_1_);
	}
}
