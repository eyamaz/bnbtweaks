package eyamaz.bnbtweaks.asm.module;

import static org.objectweb.asm.Opcodes.*;
import org.objectweb.asm.tree.*;
import squeek.asmhelper.bnbtweaks.ASMHelper;
import squeek.asmhelper.bnbtweaks.ObfHelper;
import eyamaz.bnbtweaks.BnBTweaks;
import eyamaz.bnbtweaks.asm.IClassTransformerModule;

public class ModuleDisableStrongholds implements IClassTransformerModule
{
	@Override
	public String[] getClassesToTransform()
	{
		return new String[]{
		"net.minecraft.world.gen.structure.MapGenStronghold"
		};
	}

	@Override
	public String getModuleName()
	{
		return "disableStrongholds";
	}

	@Override
	public boolean canBeDisabled()
	{
		return true;
	}

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes)
	{
		ClassNode classNode = ASMHelper.readClassFromBytes(bytes);

		if (transformedName.equals("net.minecraft.world.gen.structure.MapGenStronghold"))
		{
			MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, !ObfHelper.isObfuscated() ? "canSpawnStructureAtCoords" : "func_75047_a", "(II)Z");
			if (methodNode != null)
			{
				injectReturn(methodNode);
			}
			else
				throw new RuntimeException("Could not find canSpawnStructureAtCoords method in " + transformedName);

			return ASMHelper.writeClassToBytes(classNode);
		}
		return bytes;
	}

	private void injectReturn(MethodNode method)
	{
		AbstractInsnNode target = ASMHelper.findFirstInstruction(method);

		if (target == null)
			throw new RuntimeException("Could not find target Insn in MapGenStronghold." + method.name + " patch");

		InsnList toInject = new InsnList();

		toInject.add(new InsnNode(ICONST_0));
		toInject.add(new InsnNode(IRETURN));

		method.instructions.insertBefore(target, toInject);

		BnBTweaks.Log.info("MapGenStronghold." + method.name + " patch successfull");
	}
}