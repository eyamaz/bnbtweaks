package eyamaz.bnbtweaks.asm.module;

import static org.objectweb.asm.Opcodes.*;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.tree.*;
import squeek.asmhelper.bnbtweaks.ASMHelper;
import squeek.asmhelper.bnbtweaks.ObfHelper;
import eyamaz.bnbtweaks.asm.IClassTransformerModule;

public class ModuleSpawnerLightLevels implements IClassTransformerModule
{
	@Override
	public String[] getClassesToTransform()
	{
		return new String[]{
		"net.minecraft.tileentity.MobSpawnerBaseLogic",
		"net.minecraft.entity.monster.EntityMob",
		"net.minecraft.entity.EntityLiving",
		"lycanite.lycanitesmobs.api.entity.EntityCreatureBase"
		};
	}

	@Override
	public String getModuleName()
	{
		return "makeMobSpawnersIgnoreLightLevels";
	}

	@Override
	public boolean canBeDisabled()
	{
		return true;
	}

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes)
	{
		ClassNode classNode = ASMHelper.readClassFromBytes(bytes);

		if (transformedName.equals("net.minecraft.tileentity.MobSpawnerBaseLogic"))
		{
			MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, !ObfHelper.isObfuscated() ? "updateSpawner" : "func_98278_g", "()V");
			if (methodNode != null)
			{
				setIsSpawningFromSpawnerTrue(methodNode);
			}
			else
				throw new RuntimeException("Could not find updateSpawner method in MobSpawnerBaseLogic");

			return ASMHelper.writeClassToBytes(classNode);
		}
		else if (transformedName.equals("net.minecraft.entity.EntityLiving"))
		{
			createIsSpawningFromSpawner(classNode);

			MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, "<init>", "(Lnet/minecraft/world/World;)V");
			if (methodNode != null)
			{
				setIsSpawningFromSpawnerFalse(methodNode);
			}
			else
				throw new RuntimeException("Could not find <init> method in " + transformedName);

			return ASMHelper.writeClassToBytes(classNode);
		}
		else if (transformedName.equals("net.minecraft.entity.monster.EntityMob"))
		{
			MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, !ObfHelper.isObfuscated() ? "getCanSpawnHere" : "func_70601_bi", "()Z");
			if (methodNode != null)
			{
				makeEntityMobIgnoreLightLevel(methodNode);
			}
			else
				throw new RuntimeException("Could not find getCanSpawnHere method in EntityMob");

			methodNode = ASMHelper.findMethodNodeOfClass(classNode, !ObfHelper.isObfuscated() ? "getBlockPathWeight" : "func_70783_a", "(III)F");
			if (methodNode != null)
			{
				makeEntityMobIgnoreWorldLightLevel(methodNode);
			}
			else
				throw new RuntimeException("Could not find getBlockPathWeight method in EntityMob");

			return ASMHelper.writeClassToBytes(classNode);
		}
		/*else if (name.equals("lycanite.lycanitesmobs.api.entity.EntityCreatureBase"))
		{
			MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, "fixedSpawnCheck", "(Lnet/minecraft/world/World;III)Z");
			if (methodNode != null)
			{
				makeLycanitesMobsIgnoreLightLevel(methodNode);
			}
			else
				throw new RuntimeException("Could not find fixedSpawnCheck in EntityCreatureBase");

			return ASMHelper.writeClassToBytes(classNode);
		}*/
		return bytes;
	}

	private void createIsSpawningFromSpawner(ClassNode classNode)
	{

		FieldVisitor fv = classNode.visitField(ACC_PUBLIC, "isSpawningFromSpawner", "Z", null, null);

		fv.visitEnd();
	}

	public void setIsSpawningFromSpawnerTrue(MethodNode method)
	{
		//set entityliving.isSpawningFromSpawner = true; right before getCanSpawnHere call
		InsnList toFind = new InsnList();
		toFind.add(new MethodInsnNode(INVOKEVIRTUAL, "net/minecraft/entity/EntityLiving", "func_70601_bi", "()Z"));

		AbstractInsnNode foundStart = ASMHelper.find(method.instructions, toFind);
		AbstractInsnNode target = ASMHelper.move(foundStart, -6);

		if (foundStart == null || target == null)
			throw new RuntimeException("Unexpected instruction pattern in MobSpawnerBaseLogic.updateSpawner");

		InsnList toInject = new InsnList();
		toInject.add(new VarInsnNode(ALOAD, 15));
		LabelNode label = new LabelNode();
		toInject.add(new JumpInsnNode(IFNULL, label));
		toInject.add(new VarInsnNode(ALOAD, 15));
		toInject.add(new InsnNode(ICONST_1));
		toInject.add(new FieldInsnNode(PUTFIELD, "net/minecraft/entity/EntityLiving", "isSpawningFromSpawner", "Z"));
		toInject.add(label);

		method.instructions.insert(target, toInject);
	}

	private void setIsSpawningFromSpawnerFalse(MethodNode method)
	{
		InsnList toFind = new InsnList();
		toFind.add(new TypeInsnNode(NEW, "net/minecraft/entity/ai/EntityAITasks"));

		AbstractInsnNode target = ASMHelper.findLastInstructionWithOpcode(method, RETURN);

		if (target == null)
			throw new RuntimeException("Unexpected instruction pattern in EntityLiving.<init>");

		InsnList toInject = new InsnList();

		toInject.add(new VarInsnNode(ALOAD, 0));
		toInject.add(new InsnNode(ICONST_0));
		toInject.add(new FieldInsnNode(PUTFIELD, "net/minecraft/entity/EntityLiving", "isSpawningFromSpawner", "Z"));

		method.instructions.insertBefore(target, toInject);
	}

	public void makeEntityMobIgnoreLightLevel(MethodNode method)
	{
		//Inject hook to create
		//return this.worldObj.difficultySetting > 0 && (this.isSpawningFromSpawner || this.isValidLightLevel()) && super.getCanSpawnHere();

		InsnList toFind = new InsnList();
		toFind.add(new VarInsnNode(ALOAD, 0));
		toFind.add(new MethodInsnNode(INVOKEVIRTUAL, "net/minecraft/entity/monster/EntityMob", !ObfHelper.isObfuscated() ? "isValidLightLevel" : "func_70814_o", "()Z"));
		toFind.add(new JumpInsnNode(IFEQ, new LabelNode()));

		AbstractInsnNode foundStart = ASMHelper.find(method.instructions, toFind);
		AbstractInsnNode foundEnd = ASMHelper.move(foundStart, toFind.size() - 1);

		if (foundStart == null || foundEnd == null)
			throw new RuntimeException("Unexpected instruction pattern in EntityMob.getCanSpawnHere");

		InsnList addedCondition = new InsnList();

		addedCondition.add(new VarInsnNode(ALOAD, 0));
		addedCondition.add(new FieldInsnNode(GETFIELD, "net/minecraft/entity/monster/EntityMob", "isSpawningFromSpawner", "Z"));
		LabelNode label = new LabelNode();
		addedCondition.add(new JumpInsnNode(IFNE, label));

		method.instructions.insertBefore(foundStart, addedCondition);
		method.instructions.insert(foundEnd, label);
	}

	public void makeEntityMobIgnoreWorldLightLevel(MethodNode method)
	{
		AbstractInsnNode targetNode = ASMHelper.findFirstInstruction(method);

		if (targetNode == null)
			throw new RuntimeException("Could not find target node for EntityMob." + method.name + " patch");

		InsnList toInject = new InsnList();

		/*
		// equivalent to:
		if (this.isSpawningFromSpawner)
			return 0f;
		*/

		toInject.add(new VarInsnNode(ALOAD, 0));
		toInject.add(new FieldInsnNode(GETFIELD, "net/minecraft/entity/monster/EntityMob", "isSpawningFromSpawner", "Z"));
		LabelNode label = new LabelNode();
		toInject.add(new JumpInsnNode(IFEQ, label));
		toInject.add(new InsnNode(FCONST_0));
		toInject.add(new InsnNode(FRETURN));
		toInject.add(label);

		method.instructions.insertBefore(targetNode, toInject);
	}

	// TODO: verify that this still works
	public void makeLycanitesMobsIgnoreLightLevel(MethodNode method)
	{
		AbstractInsnNode firstTargetNode = ASMHelper.findFirstInstructionWithOpcode(method, IF_ICMPLE);
		AbstractInsnNode secondTargetNode = ASMHelper.findFirstInstructionWithOpcode(method, IF_ICMPGE);
		AbstractInsnNode thirdTargetNode = ASMHelper.findFirstInstructionWithOpcode(method, IRETURN);
		AbstractInsnNode fourthTargetNode = ASMHelper.findNextInstructionWithOpcode(thirdTargetNode, IRETURN);

		if (firstTargetNode == null || secondTargetNode == null || thirdTargetNode == null || fourthTargetNode == null)
			throw new RuntimeException("Could not find target node for EntityCreatureBase" + method.name + "patch");

		InsnList firstInject = new InsnList();
		InsnList secondInject = new InsnList();
		InsnList firstLabelInject = new InsnList();
		InsnList secondLabelInject = new InsnList();

		/*
		 * Equivalent to:                                           |          Injection             |
		if(this.spawnsInDarkness && this.testLightLevel(i, j, k) > 1 && !Hooks.isSpawningFromSpawner)
			return false;
		if(this.spawnsOnlyInLight && this.testLightLevel(i, j, k) < 2 && !Hooks.isSpawningFromSpawner)
			return false;
		*/

		firstInject.add(new FieldInsnNode(GETSTATIC, "eyamaz/bnbtweaks/asm/Hooks", "isSpawningFromSpawner", "Z"));
		LabelNode label1 = new LabelNode();
		firstInject.add(new JumpInsnNode(IFNE, label1));

		secondInject.add(new FieldInsnNode(GETSTATIC, "eyamaz/bnbtweaks/asm/Hooks", "isSpawningFromSpawner", "Z"));
		LabelNode label2 = new LabelNode();
		secondInject.add(new JumpInsnNode(IFNE, label2));

		firstLabelInject.add(label1);
		secondLabelInject.add(label2);

		method.instructions.insert(firstTargetNode, firstInject);
		method.instructions.insert(secondTargetNode, secondInject);
		method.instructions.insert(thirdTargetNode, firstLabelInject);
		method.instructions.insert(fourthTargetNode, secondLabelInject);
	}
}
