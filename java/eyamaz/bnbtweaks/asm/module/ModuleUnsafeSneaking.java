package eyamaz.bnbtweaks.asm.module;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.tree.*;
import squeek.asmhelper.bnbtweaks.ASMHelper;
import squeek.asmhelper.bnbtweaks.ObfHelper;
import eyamaz.bnbtweaks.asm.Hooks;
import eyamaz.bnbtweaks.asm.IClassTransformerModule;

public class ModuleUnsafeSneaking implements IClassTransformerModule
{

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes)
	{
		ClassNode classNode = ASMHelper.readClassFromBytes(bytes);

		MethodNode method = ASMHelper.findMethodNodeOfClass(classNode, !ObfHelper.isObfuscated() ? "moveEntity" : "func_70091_d", "(DDD)V");
		if (method != null)
		{
			InsnList needle = new InsnList();
			needle.add(new VarInsnNode(Opcodes.ALOAD, 0));
			needle.add(new FieldInsnNode(Opcodes.GETFIELD, "net/minecraft/entity/Entity", !ObfHelper.isObfuscated() ? "onGround" : "field_70122_E", "Z"));
			needle.add(new JumpInsnNode(Opcodes.IFEQ, new LabelNode()));
			needle.add(new VarInsnNode(Opcodes.ALOAD, 0));
			needle.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "net/minecraft/entity/Entity", !ObfHelper.isObfuscated() ? "isSneaking" : "func_70093_af", "()Z"));
			needle.add(new JumpInsnNode(Opcodes.IFEQ, new LabelNode()));
			needle.add(new VarInsnNode(Opcodes.ALOAD, 0));
			needle.add(new TypeInsnNode(Opcodes.INSTANCEOF, "net/minecraft/entity/player/EntityPlayer"));
			needle.add(new JumpInsnNode(Opcodes.IFEQ, new LabelNode()));
			needle.add(new InsnNode(Opcodes.ICONST_1));
			needle.add(new JumpInsnNode(Opcodes.GOTO, new LabelNode()));

			AbstractInsnNode firstFoundInsn = ASMHelper.find(method.instructions, needle);
			if (firstFoundInsn == null)
				throw new RuntimeException("Failed to find expected pattern in Entity.moveEntity");

			LabelNode ifFailed = ((JumpInsnNode) firstFoundInsn.getNext().getNext()).label;

			InsnList toInject = new InsnList();
			toInject.add(new VarInsnNode(Opcodes.ALOAD, 0));
			toInject.add(new MethodInsnNode(Opcodes.INVOKESTATIC, Type.getInternalName(Hooks.class), "canSneakOffLedge", "(Lnet/minecraft/entity/Entity;)Z"));
			toInject.add(new JumpInsnNode(Opcodes.IFNE, ifFailed));
			method.instructions.insertBefore(firstFoundInsn, toInject);
		}
		else
			throw new RuntimeException("Could not find Entity.moveEntity method");

		return ASMHelper.writeClassToBytes(classNode);
	}

	@Override
	public String[] getClassesToTransform()
	{
		return new String[]{"net.minecraft.entity.Entity"};
	}

	@Override
	public String getModuleName()
	{
		return "makeSneakingUnsafe";
	}

	@Override
	public boolean canBeDisabled()
	{
		return true;
	}

}
