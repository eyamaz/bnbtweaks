package eyamaz.bnbtweaks.asm.module;

import static org.objectweb.asm.Opcodes.*;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.tree.*;

import squeek.asmhelper.bnbtweaks.ASMHelper;
import eyamaz.bnbtweaks.BnBTweaks;
import eyamaz.bnbtweaks.asm.IClassTransformerModule;

public class ModulePassthroughLeaves implements IClassTransformerModule
{
	@Override
	public String[] getClassesToTransform()
	{
		return new String[]{
		"net.minecraft.block.BlockLeaves"
		};
	}

	@Override
	public String getModuleName()
	{
		return "passthroughLeaves";
	}

	@Override
	public boolean canBeDisabled()
	{
		return true;
	}

	@Override
	public byte[] transform(String name, String transformedName, byte[] bytes)
	{
		ClassNode classNode = ASMHelper.readClassFromBytes(bytes);

		if (transformedName.equals("net.minecraft.block.BlockLeaves"))
		{
			removeCollisionBox(classNode);
			verifyMethodAdded(classNode, "func_149668_a", "(Lnet/minecraft/world/World;III)Lnet/minecraft/util/AxisAlignedBB;");

			return ASMHelper.writeClassToBytes(classNode);
		}
		return bytes;
	}
	
	public void verifyMethodAdded(ClassNode classNode, String name, String desc)
	{
		MethodNode methodNode = ASMHelper.findMethodNodeOfClass(classNode, name, desc);
		if (methodNode != null)
		{
			BnBTweaks.Log.info("Successfully added method: " + methodNode.name + methodNode.desc);
		}
		else
			throw new RuntimeException("Could not create method " + name + desc + " in: " + classNode.name);
	}
	
	private void removeCollisionBox(ClassNode classNode)
	{
		MethodVisitor visitor = classNode.visitMethod(ACC_PUBLIC, "func_149668_a", "(Lnet/minecraft/world/World;III)Lnet/minecraft/util/AxisAlignedBB;", null, null);
		visitor.visitCode();
		visitor.visitInsn(ACONST_NULL);
		visitor.visitInsn(ARETURN);
		visitor.visitEnd();
	}
}