BnBTweaks
=========

A Minecraft mod that makes various modifications to the game and/or other mods for the 1.7.10 Blood N' Bones modpack

### Current tweaks made:
- Makes mob spawners ignore light levels
- Disables Strongholds
- Disables Mineshafts
- Stops sneaking from preventing falling off ledges
- Allows leaf blocks to be passed through